# Requirements
1. start SteamVR as Admin (for access of camera needed)
2. start Unity as Admin too (for init of OpenVR)

# download
[https://developer.vive.com/resources/knowledgebase/vive-srworks-sdk/](https://developer.vive.com/resources/knowledgebase/vive-srworks-sdk/)

# install / Startup
1. import unity package
1.a if "Error: The name 'Registry' does not exist in current context" -> PlayerSettings -> otherSettings -> Api Compatibility Level: .NET 4.x
2. delete Assets/ViveSR/Plugins/openvr_api.dll
3. opoen Sample -> Assets/ViveSr/Scenes/Sample/Sample
4. Accept OpenVR support && Layer
5. Play

